Assignment 2 only contains Declarative sequential model while Assignment 3 also has Declarative concurrency.
1.  Declarative sequential model includes features such as variable binding, records, pattern recognition, conditionals, procedures and recursion
2.  Declarative sequential model was extended to deal with concurrency with feature of Threads

Useful Links:
Cell- 
https://mozart.github.io/mozart-v1/doc-1.4.0/tutorial/node9.html#section.stateful.cell
http://www.cse.iitk.ac.in/users/cs350/2011/explicit-state.html

POPL TEAM-
Arnab
Sharbatanu
Viveka
